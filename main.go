package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

func main() {

	http.HandleFunc("/", countPage())
	http.HandleFunc("/input", rec())
	http.ListenAndServe(":8090", nil)

	for {
		time.Sleep(100 * time.Hour)
	}
}

func countPage() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		finalPage, _ := ioutil.ReadFile("page.html")

		fmt.Fprintf(w, string(finalPage))
	}
}

func rec() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		//fmt.Println("Working")
		//fmt.Println(r.FormValue("ajax_post_data"))
		f, err := os.OpenFile("logfile.csv", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Println(err)
		}
		defer f.Close()

		tNow := strconv.Itoa(int(time.Now().Unix()))

		if _, err := f.WriteString(tNow + r.FormValue("ajax_post_data") + ",\n"); err != nil {
			log.Println(err)
		}
	}
}
