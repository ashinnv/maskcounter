package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

type count struct {
	TimeStamp int
	Mask      bool
	Whit      bool
	Male      bool
}

type report struct {
	TotalPeople int

	WhiteWithMask int
	TotalWhite    int

	NwhitewitMask int
	TotalNonWhite int

	TotalFemale    int
	FemaleWithMask int

	TotalMale    int
	MaleWithMask int
}

func main() {
	file, _ := os.Open("logfile.csv")
	rawData, _ := ioutil.ReadAll(file)
	lines := strings.Split(string(rawData), "\n")
	var structs []count

	for _, line := range lines {

		tmp, err := popStruct(line)
		if err != nil {
			fmt.Print("")
		} else {
			structs = append(structs, tmp)
		}

	}

	rept := generateReport(structs)

	fmt.Printf("Total People Encountered, White People With Mask,Total White,Non-White With Mask,Total Non-White, Female With Mask, Total Females, Males With Masks, Total Males\n%d,%d,%d,%d,%d,%d,%d,%d,%d", rept.TotalPeople, rept.WhiteWithMask, rept.TotalWhite, rept.NwhitewitMask, rept.TotalNonWhite, rept.FemaleWithMask, rept.TotalFemale, rept.MaleWithMask, rept.TotalMale)

}

func murderLine(input string) string {
	if len(input) < 1 {
		return ""
	}

	numberLine := input[:10]
	tmp := input[10:]

	tmp = strings.Replace(tmp, "_", ",", 20)
	return numberLine + "," + tmp
}

func popStruct(input string) (count, error) {
	var retCount count
	var err error

	if input == "" || input == " " {
		err = errors.New("Empty String Passed to PopStruct")
		return retCount, err
	}

	numberLine := input[:10]
	followLine := input[10:]

	retCount.TimeStamp, _ = strconv.Atoi(numberLine)

	if strings.Contains(followLine, "white") || strings.Contains(followLine, "while") {
		retCount.Whit = true
	} else if strings.Contains(followLine, "black") {
		retCount.Whit = false
	}

	if strings.Contains(followLine, "female") {
		retCount.Male = false
	} else if strings.Contains(followLine, "male") {
		retCount.Male = true
	}

	if strings.Contains(followLine, "face") {
		retCount.Mask = false
	} else if strings.Contains(followLine, "mask") {
		retCount.Mask = true
	}

	return retCount, nil

}

func generateReport(input []count) report {

	var rept report = report{0, 0, 0, 0, 0, 0, 0, 0, 0}

	// Count total people
	rept.TotalPeople = len(input)

	for _, ln := range input {

		if ln.Whit {
			rept.TotalWhite++

			if ln.Mask {
				rept.WhiteWithMask++

			}

		} else if !ln.Whit {
			rept.TotalNonWhite++
			if ln.Mask {
				rept.NwhitewitMask++
			}
		}

	}

	for _, ln := range input {

		if ln.Male {
			if ln.Mask {
				rept.MaleWithMask++
				rept.TotalMale++
			} else {
				rept.TotalMale++
			}
		} else {
			if ln.Mask {
				rept.FemaleWithMask++
				rept.TotalFemale++
			} else {
				rept.TotalFemale++
			}
		}

	}

	return rept

}
